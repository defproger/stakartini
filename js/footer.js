/* ---- particles.js config ---- */

particlesJS("dust", {
    "particles": {
        "number": {
            "value": 10,
            "density": {
                "enable": true,
                "value_area": 500
            }
        },
        "color": {
            "value": "#242424"
        },
        "shape": {
            "type": "circle",
            "stroke": {
                "width": 0,
                "color": "#242424"
            },
            "image": {
                // "src": "img/github.svg",
                "width": 100,
                "height": 1
            }
        },
        "opacity": {
            "value": 1,
            "random": false,
            "anim": {
                "enable": false,
                "speed": 0,
                "opacity_min": 1,
                "sync": false
            }
        },
        "size": {
            "value": 100,
            "random": true,
            "anim": {
                "enable": true,
                "speed": 2,
                "size_min": 70,
                "sync": false
            }
        },
        "line_linked": {
            "enable": false
        },
        "move": {
            "enable": true,
            "speed": 2,
            "direction": "none",
            "random": true,
            "straight": false,
            "out_mode": "out",
            "bounce": false,
            "attract": {
                "enable": false,
                "rotateX": 600,
                "rotateY": 1200
            }
        }
    },
    "interactivity": {
        "detect_on": "canvas",
        "events": {
            "onhover": {
                "enable": false,
            },
            "onclick": {
                "enable": false
            },
            "resize": true
        }
    },
    "retina_detect": true
});